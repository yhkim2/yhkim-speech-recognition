      // 환자 이름값 저장
      var temp;
      function input(){
      var input = document.getElementById("name").value;
      temp = input;
      }

      // 생성된 text txt파일로 저장
      function downloadInnerHtml(elementid, htmllinereplace, filename, mimeType, extension) {
        var elHtml = $(elementid).html();
        if (htmllinereplace) elHtml = elHtml.replace(/(<([^>]+)>)/ig,"");
        var link = document.createElement('a');
        link.setAttribute('download', filename + extension);
        link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));
        link.click();
      }

      // 음성을 텍스트로 변환
      window.addEventListener("DOMContentLoaded", () => {
        const recordButton = document.getElementById("recordButton");
        const stopButton = document.getElementById("stopButton");
        const result = document.getElementById("result");
        const main = document.getElementsByTagName("main")[0];
        let listening = false;

        // 구글 기반 음성인식 기능 불러오기 (타 브라우저 사용불가)
        const SpeechRecognition =
          window.SpeechRecognition || window.webkitSpeechRecognition;
        if (typeof SpeechRecognition !== "undefined") {
          const recognition = new SpeechRecognition();

          const stop = () => {
            main.classList.remove("speaking");
            recognition.stop();
          };

          const start = () => {
            main.classList.add("speaking");
            recognition.start();
          };

          // 인식한 음성을 p클래스에 text로 저장
          const onResult = event => {
            result.innerHTML = "";
            for (const res of event.results) {
              const text = document.createTextNode(res[0].transcript);
              const p = document.createElement("p");
              if (res.isFinal) {
                p.classList.add("final");
              }
              p.appendChild(text);
              result.appendChild(p);
            }
          };
          recognition.continuous = true;
          recognition.interimResults = true;
          recognition.addEventListener("result", onResult);
          recordButton.addEventListener("click", event => {
            if(listening == false) {
              start();
              listening = !listening;
            }
          });
          stopButton.addEventListener("click", event => {
            if(listening == true) {
              stop();
              listening = !listening;
            }
          });
        } else {
          recordButton.remove();
          const message = document.getElementById("message");
          message.removeAttribute("hidden");
          message.setAttribute("aria-hidden", "false");
        }
      });