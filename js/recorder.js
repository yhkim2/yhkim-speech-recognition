// webkitURLd을 사용하지 않지만 에러가 날 수 있기에 불러온다
URL = window.URL || window.webkitURL;

var gumStream; 						// 사용자의 카메라와 마이크 스트림 연결하는 API
var recorder; 						// 사용자의 음성을 저장할 객체
var chunks = [];					// 녹음 후 브라우저에 데이터 저장할 배열
var extension;

var recordButton = document.getElementById("recordButton");
var stopButton = document.getElementById("stopButton");
var pauseButton = document.getElementById("pauseButton");

recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);
pauseButton.addEventListener("click", pauseRecording);

//녹음 기능 사용시 브라우저 호환성 검사
console.log("audio/webm:"+MediaRecorder.isTypeSupported('audio/webm;codecs=opus'));
console.log("audio/ogg:"+MediaRecorder.isTypeSupported('audio/ogg;codecs=opus'));
if (MediaRecorder.isTypeSupported('audio/webm;codecs=opus')){
	extension="webm";
}else{
	extension="ogg"
}

// 음성 녹음 시작
function startRecording() {
	console.log("recordButton clicked");

	// 객체에 간단한 제약조건
    var constraints = {audio: true}

	recordButton.disabled = true;
	stopButton.disabled = false;
	pauseButton.disabled = false

	navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
		console.log("getUserMedia() success, stream created, initializing MediaRecorder");

		// gumStream 사용을 위해 변수지정
		gumStream = stream;

		// 음성 녹음 옵션 지정
		var options = {
	      audioBitsPerSecond :  256000,
	      videoBitsPerSecond : 2500000,
	      bitsPerSecond:       2628000,
	      mimeType : 'audio/'+extension+';codecs=opus'
	    }

	    // 음성 녹음 정보 업데이트 
		document.getElementById("formats").innerHTML='Sample rate: 48kHz, MIME: audio/'+extension+';codecs=opus';

		// MediaRecorder 객체 생성
		recorder = new MediaRecorder(stream, options);

	    recorder.ondataavailable = function(e){
	    	console.log("recorder.ondataavailable:" + e.data);
	    	
	    	console.log ("recorder.audioBitsPerSecond:"+recorder.audioBitsPerSecond)
	    	console.log ("recorder.videoBitsPerSecond:"+recorder.videoBitsPerSecond)
	    	console.log ("recorder.bitsPerSecond:"+recorder.bitsPerSecond)
	      	// 녹음한 음성을 정크에 추가
	      	chunks.push(e.data);
	      	// 레코더가 비활성이면 녹화가 완료
	      	if (recorder.state == 'inactive') {
	          // 녹음된 음성이 있는 정크를 Blob을 이용하여 webm 오디오형식으로 변환
	          const blob = new Blob(chunks, { type: 'audio/'+extension, bitsPerSecond:128000});
	          createDownloadLink(blob)
	      	}
	    };

	    recorder.onerror = function(e){
	    	console.log(e.error);
	    }
	    recorder.start(1000);

    }).catch(function(err) {
	  	// getUserMedia() 객체 사용이 안될때 레코드 버튼 활성화
    	recordButton.disabled = false;
    	stopButton.disabled = true;
    	pauseButton.disabled = true
	});
}

// 음성 녹음 일시정지/다시녹음
function pauseRecording(){
	console.log("pauseButton clicked recorder.state=",recorder.state );
	if (recorder.state=="recording"){
		// 잠시멈춤
		recorder.pause();
		pauseButton.innerHTML="Resume";
	}else if (recorder.state=="paused"){
		// 다시시작
		recorder.resume();
		pauseButton.innerHTML="Pause";

	}
}

// 음성 녹음 정지
function stopRecording() {
	console.log("stopButton clicked");

	stopButton.disabled = true;
	recordButton.disabled = false;
	pauseButton.disabled = true;

	// 녹화가 일시 중지된 상태에서 버튼 재설정
	pauseButton.innerHTML="Pause";
	
	// 녹화 종료
	recorder.stop();

	// 마이크 기능 정지
	gumStream.getAudioTracks()[0].stop();
}

// 음성 파일 다운로드
function createDownloadLink(blob) {
	
	var url = URL.createObjectURL(blob);
	var au = document.createElement('audio');
	var li = document.createElement('li');
	var link = document.createElement('a');

	// 오디오 요소에 컨트롤 기능 추가
	au.controls = true;
	au.src = url;

	// 오디오 요소에 Blob 연결
	link.href = url;
	link.download = new Date().toISOString() + '.'+extension;
	link.innerHTML = link.download;

	// 리스트 요소에 컨트롤러와 다운로드링크 추가
	li.appendChild(au);
	li.appendChild(link);
	recordingsList.appendChild(li);
}